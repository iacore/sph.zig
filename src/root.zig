const std = @import("std");
const testing = std.testing;
const Allocator = std.mem.Allocator;
const Random = std.rand.Random;

test "Table Of Contents" {
    _ = .{
        // model essentials
        Actor, // tr started
        Decoder, // tbd
        Encoder, // tbd
        Hierarchy, // tbd
        // special encoders
        Image_Encoder, // tbd
    };
}

const pi = std.math.pi;
const pi2 = pi * 2;
const pi_over_2 = pi / 2;
const log2_e = std.math.log2e;
const log2_e_inv = 1.0 / log2_e;

// magic numbers
const exp_iters = 6;
const log_iters = 6;
const trig_iters = 6;
const limit_min = -1e6;
const limit_max = 1e6;
const limit_small = 1e-6;
// const rand_subseed_offset = 12345;
const init_weight_noisei = 9;
const init_weight_noisef = 0.01;

// const Int2 = @Vector(2, i32);
// const Int3 = @Vector(3, i32);
// const Int4 = @Vector(4, i32);
// const Float2 = @Vector(2, f32);
// const Float3 = @Vector(3, f32);
// const Float4 = @Vector(4, f32);
// const Byte = u8;
// const S_Byte = i8;

// const Byte_Buffer = Array(u8);
// const S_Byte_Buffer = Array(i8);
// const U_Short_Buffer = Array(u16);
// const Short_Buffer = Array(i16);
// const U_Int_Buffer = Array(u32);
// const Int_Buffer = Array(i32);
// const Float_Buffer = Array(f32);

pub const Array = std.ArrayList;
pub const Circle_Buffer = @import("helpers.zig").Circle_Buffer;

/// CSDR layer size
pub const LayerSize = struct {
    x: usize,
    y: usize,
    z: usize,
};
/// Column Position
pub const CPos = struct {
    x: u32,
    y: u32,
};

/// CSDR columns
pub const is = []i32;
/// boring floating point weights
pub const fs = []f32;
pub const is_view = []const i32;
pub const fs_view = []const f32;
pub fn new_ints(a: Allocator, len: usize) !is {
    const xs = try a.alloc(i32, len);
    @memset(xs, 0);
    return xs;
}
pub fn new_floats(a: Allocator, len: usize) !fs {
    const xs = try a.alloc(f32, len);
    @memset(xs, 0);
    return xs;
}

/// randomize float weights
pub fn initialize_floats_to_random(rng: Random, xs: fs) void {
    for (xs) |*x| {
        // optimize: fix mantissa bits and set magnitude directly
        x.* = (rng.float(f32) * 2 - 1) * init_weight_noisef;
    }
}

pub const Actor = struct {
    pub const VisibleLayerDesc = struct {
        size: LayerSize = .{ .x = 4, .y = 4, .z = 16 },
        /// radius of 2 creates a 5x5 convolutional filter with sparse columns
        radius: usize = 2,
    };

    pub const Visible_Layer = struct {
        value_weights: fs,
        action_weights: fs,
    };

    pub const History_Sample = struct {
        input_cis: []is,
        hidden_target_cis_prev: is,
        reward: f32,
    };

    pub const Params = struct {
        vlr: f32 = 0.01,
        alr: f32 = 0.01,
        discount: f32 = 0.99,
        min_steps: usize = 8,
        history_iters: usize = 8,
    };

    _arena: std.heap.ArenaAllocator,
    rng: Random,

    hidden_size: LayerSize,
    history_size: usize,
    hidden_acts: fs,
    hidden_cis: is,
    hidden_values: fs,
    history_samples: Circle_Buffer(History_Sample),
    visible_layers: []Visible_Layer,
    visible_layer_descs: []const VisibleLayerDesc,

    test {
        var prng = std.rand.DefaultPrng.init(0);
        const actor = try init_random(testing.allocator, prng.random(), .{ .x = 4, .y = 4, .z = 16 }, 10, &.{ .{}, .{}, .{}, .{} });
        defer actor.deinit();
    }

    /// allocate memory and nudge some weights a bit off 0. nothing fancy.
    pub fn init_random(allocator: Allocator, rng: Random, hidden_size: LayerSize, history_capacity: usize, visible_layer_descs: []const VisibleLayerDesc) !@This() {
        var arena = std.heap.ArenaAllocator.init(allocator);
        errdefer arena.deinit();
        const a = arena.allocator();

        const visible_layers = try a.alloc(Visible_Layer, visible_layer_descs.len);

        const num_hidden_columns = hidden_size.x * hidden_size.y;
        const num_hidden_cells = num_hidden_columns * hidden_size.z;

        for (visible_layer_descs, visible_layers) |vld, *vl| {
            // create weight matrix for this visible layer and initialize randomly

            const diam = vld.radius * 2 + 1;
            const area = diam * diam;

            vl.value_weights = try new_floats(a, num_hidden_columns * area * vld.size.z);

            initialize_floats_to_random(rng, vl.value_weights);

            vl.action_weights = try new_floats(a, num_hidden_cells * area * vld.size.z);

            initialize_floats_to_random(rng, vl.action_weights);
        }

        const hidden_cis = try new_ints(a, num_hidden_columns);
        const hidden_values = try new_floats(a, num_hidden_columns);
        const hidden_acts = try new_floats(a, num_hidden_cells);

        const history_samples = try Circle_Buffer(History_Sample).init(a, history_capacity);

        for (history_samples.items) |*item| {
            item.input_cis = try a.alloc(is, visible_layers.len);

            for (visible_layer_descs, item.input_cis) |vld, *layer| {
                const num_visible_columns = vld.size.x * vld.size.y;

                layer.* = try new_ints(a, num_visible_columns);
            }

            item.hidden_target_cis_prev = try new_ints(a, num_hidden_columns);
        }

        return @This(){
            ._arena = arena,
            ._rng = rng,
            .visible_layers = visible_layers,
            .visible_layer_descs = visible_layer_descs,
            .hidden_size = hidden_size,
            .hidden_cis = hidden_cis,
            .hidden_values = hidden_values,
            .hidden_acts = hidden_acts,
            .history_size = 0,
            .history_samples = history_samples,
        };
    }

    pub fn deinit(this: @This()) void {
        this._arena.deinit();
    }

    pub fn clear_state(this: *@This()) void {
        @memset(this.hidden_cis, 0);
        @memset(this.hidden_values, 0);
        this.history_size = 0;
    }

    pub fn step(this: *@This(), input_cis: []const is_view, hidden_target_cis_prev: is_view, reward: f32, learn_enabled: bool, mimic: f32, params: Params) void {

        // forward kernel

        const base_seed = this.rng.int(u64);

        // optimize: PARALLEL_FOR
        for (0..this.hidden_size.x) |x|
            for (0..this.hidden_size.y) |y| {
                var prng = std.rand.DefaultPrng.init(base_seed +% (x * this.hidden_size.y + y));
                this.forward(.{ .x = x, .y = y }, input_cis, prng.random(), params);
            };

        this.history_samples.advance(-1);

        // if not at cap, increment
        if (this.history_size < this.history_samples.len())
            this.history_size += 1;

        // add new sample
        {
            const s = this.history_samples.ref(0);

            for (s.input_cis, input_cis) |*a, b|
                a.* = b;

            // copy
            s.hidden_target_cis_prev = hidden_target_cis_prev;

            s.reward = reward;
        }

        // learn (if have sufficient samples)
        if (learn_enabled and this.history_size > params.min_steps) {
            for (0..params.history_iters) |it| {
                _ = it;

                const t = this.rng.int(i32) % (this.history_size - params.min_steps) + params.min_steps;

                // compute (partial) values, rest is completed in the kernel
                var r: f32 = 0.0;
                var d: f32 = 1.0;

                var t2 = t - 1; // note: this seems to touch first t columns
                while (t2 >= 0) : (t2 -= 1) {
                    r += this.history_samples[t2].reward * d;

                    d *= params.discount;
                }

                // optimize: PARALLEL_FOR
                for (0..this.hidden_size.x) |x|
                    for (0..this.hidden_size.y) |y|
                        this.learn(.{ .x = x, .y = y }, t, r, d, mimic, params);
            }
        }
    }

    pub fn forward(this: *@This(), column_pos: CPos, input_cis: []const is_view, rng: Random, params: Params) void {
        _ = this;
        _ = column_pos;
        _ = input_cis;
        _ = params;
        const total = undefined;
        // float cusp = randf(state) * total;
        const cusp = rng.float(f32) * total;
        _ = cusp;
    }

    pub fn learn(this: *@This(), column_pos: CPos, t: i32, r: f32, d: f32, mimic: f32, params: Params) void {
        _ = this;
        _ = column_pos;
        _ = t;
        _ = r;
        _ = d;
        _ = mimic;
        _ = params;
    }
};

pub const Decoder = struct {};
pub const Encoder = struct {};
pub const Hierarchy = struct {};
pub const Image_Encoder = struct {};
