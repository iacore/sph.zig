const std = @import("std");
const Allocator = std.mem.Allocator;

/// not a circular buffer
/// hella weird
pub fn Circle_Buffer(comptime T: type) type {
    return struct {
        a: Allocator,
        items: []T,
        start: usize,

        pub fn init(a: Allocator, capacity: usize) !@This() {
            const items = try a.alloc(T, capacity);
            if (T == i32 or T == f32) {
                @memset(items, 0);
            }
            return .{
                .items = items,
                .a = a,
                .start = 0,
            };
        }

        pub fn len(this: @This()) usize {
            return this.items.len;
        }

        pub fn ref(this: @This(), i: usize) *T {
            return &this.items[@mod(this.start + i, this.len())];
        }

        pub fn advance(this: *@This(), delta: isize) void {
            this.start = @intCast(@mod(@as(isize, @intCast(this.start)) + delta, this.len()));
        }
    };
}
